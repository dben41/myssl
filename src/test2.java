
public class test2 {

	public static void main(String[] args) {
		String s = "100101110001000000101110100000100100001000001110110111101000001";
		String t = "111001000000011101000100100111000110000100001001110001010001000";
		//String xorString = s ^ t;
		
		
		
		System.out.println(XOR(s,t));

	}
	
	public static String XOR(String s, String t){
		StringBuilder sb = new StringBuilder();

		for(int i = 0; i < s.length(); i++)
		    sb.append((t.charAt(i) ^ s.charAt(i)));

		return sb.toString();	
	}

}
