import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.security.cert.Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.security.cert.CertificateException;
import javax.security.cert.X509Certificate;



public class Test {

	public static void main(String[] args) throws Exception {
		//load BobCert
		KeyStore bobKeystore = getKeystore("src/resources/updatedkeys/keystoreBob.jks");
		Certificate bobCert = bobKeystore.getCertificate("bobCert");
		
		//extract public key from BobCert
		PublicKey bobPublicKey = bobCert.getPublicKey();
		
		//encrypt something using the public key
		String something = "something";
		
		String encryptedSomething = encrypt(something, bobPublicKey);
		
		PrivateKey bobPrivateKeys = getPrivateKey("src/resources/updatedkeys/bobPrivateKey.der");
		//display to user
		hexPrint(bobPrivateKeys.getEncoded(), "Here's Bob's private key...");
		
		//decrypt using bobPrivate key
		String decryptedSomething = decrypt(encryptedSomething, bobPrivateKeys);

	}
	
	public static KeyStore getKeystore(String path){
		KeyStore temp = null;
		try {
			temp = KeyStore.getInstance("JKS");
			FileInputStream fileInput = new FileInputStream(path);
			char[] password = "changeit".toCharArray();
			temp.load(fileInput, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return temp;
		
		//KeyStore temp = SecurityUtil.getKeystore("Alice");
		//Certificate c = temp.getCertificate("bobCert");
	}
		
	/*
	 * we'll need to print hexidecimal representations of keys, and byte arrays often.
	 */
	private static void hexPrint(byte[] bytes, String element){
		//use a string buffer
		StringBuffer temp = new StringBuffer();
		//turn bytes into a stringbuffer
		for(int i =0; i< bytes.length; i++){
			int mask = 255;
			temp.append(Integer.toHexString(mask & bytes[i]));
		}
		
		 System.out.println(element + " : " + temp + "\n");
		
	}
	
	

	
	public static String encrypt(String unencryptedString, PublicKey pubKey){
		byte[] cipherText = null;
		String returnText = "";
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, pubKey);
			byte[] messageBytes = unencryptedString.getBytes("UTF8");			
			cipherText = cipher.doFinal(messageBytes);
			
			returnText = Base64.getEncoder().encodeToString(cipherText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnText;
	}
	
	public static String decrypt(String encryptedString, PrivateKey privKey){
		byte[] plainText = null;
		byte[] cipherText = null;
		String returnText = "";
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.DECRYPT_MODE, privKey);
			
			cipherText = Base64.getDecoder().decode(encryptedString);
			plainText = cipher.doFinal(cipherText);
			
			returnText = new String(plainText);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnText;
	}
	
	
	/*
	 * Helper method that retireves the PrivateKey
	 */
	private static PrivateKey getPrivateKey(String bobprivatekeyname2) {
		System.out.println("Getting private key: " + bobprivatekeyname2);
		//put the key into a java file
		File key = new File(bobprivatekeyname2);
		//error check before importing
		if(!key.exists()) {
			System.out.println("ERROR: You're missing the private key! ");
			System.exit(-1);
		}
		
		try{
			//put the bytes into an array
			FileInputStream input = new FileInputStream(key);
			DataInputStream dataInput = new DataInputStream(input);
			int fileLength = (int) key.length();
			byte[] keyByte = new byte[fileLength];
			dataInput.readFully(keyByte);
			//decode them
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyByte);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PrivateKey temp = keyFactory.generatePrivate(keySpec);
			//return the key
			return temp;
			
		}catch(FileNotFoundException e){
			System.out.println(e);
			System.exit(-1);
		}catch(IOException e){
			System.out.println(e);
			System.exit(-1);
		} catch(NoSuchAlgorithmException e){ 
			System.out.println(e);
			System.exit(-1);
		} catch(InvalidKeySpecException e){ 
			e.printStackTrace();
			System.exit(-1);
		}
		return null;
	}

}
