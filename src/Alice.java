import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.Arrays;

import javax.crypto.SecretKey;




public class Alice {
	static Boolean demostration = true;
	public static void main(String[] args) {
		String allMessages = "";
		
		try {
			vWrite("Alice: Program starting...");
			vWrite("Alice: Opening connection to Bob...");
			
			//open connection to Bob
			Socket bobSocket = new Socket("localhost",2112);
			DataOutputStream bobOutputStream = new DataOutputStream(bobSocket.getOutputStream());
			DataInputStream bobInputStream = new DataInputStream(bobSocket.getInputStream());
			vWrite("Alice: Connected! localhost:2112");
			
			/********************************************************************************
			 **************** Handshake Phase ***********************************************
			 ********************************************************************************/
			
			//load AliceCert
			KeyStore aliceKeystore = SecurityUtil.getKeystore("src/resources/updatedkeys/keystoreAlice.jks");
			vWrite("Alice: Loading alice keystore: " + aliceKeystore.toString());
			Certificate aliceCert = aliceKeystore.getCertificate("aliceCert");
			vWrite("Alice: Loading alice certicate: " + aliceCert.getEncoded());
			dWrite("/************************************************************ ");
			dWrite("******************Alice Handshake Phase********************** ");
			dWrite("/***********************************************************/ ");
			//for debugging
			//byte[] hello = aliceCert.getEncoded();
			//int size = hello.length;
			
			//send Bob Alice Cert
			/* MESSSAGE 1 */
			vWrite("Alice: Message 1: Sending certificate. ");
			dWrite("Message 1 To Bob  : { Alice Certificate, integrity protection:AES, data encryption: SHA-1 }");
			bobOutputStream.write(aliceCert.getEncoded());
			bobOutputStream.flush();
			
			//add an entry that will be used by Hash.
			allMessages += SecurityUtil.toHexString(aliceCert.getEncoded());
			
			vWrite("Alice: Message 1: Sending intregity protection and data encryption info to Bob.");
			String dataEncryption = "AES";
			String integrityProtection = "SHA-1";
			bobOutputStream.writeBytes("integrity_protection:"+integrityProtection+",dataEncryption:"+ dataEncryption + "\n");
			
			//add an entry that will be used by Hash.
			allMessages += "integrity_protection:"+integrityProtection+",dataEncryption:"+ dataEncryption;
			
			//get Bob's Cert
			byte[] bobCertBytes = new byte[646];
			bobInputStream.readFully(bobCertBytes);
			InputStream in = new ByteArrayInputStream(bobCertBytes);
			CertificateFactory certFactory = CertificateFactory.getInstance("X.509");		
			//Certificate bobCert = certFactory.generateCertificate(in);
			X509Certificate bobCert = (X509Certificate)certFactory.generateCertificate(in);
			vWrite("Alice: Message 2, receiving Bob's certificate: " + bobCert.getEncoded());
			dWrite("Message 2 From Bob: { Bob Certificate }");
			
			//add an entry that will be used by Hash.
			allMessages += SecurityUtil.toHexString(bobCertBytes);
					
			//extract public key from BobCert
			PublicKey bobPublicKey = bobCert.getPublicKey();
			vWrite("Alice: Extracting public key from Bob's certificate: " + bobPublicKey.getEncoded());
			
			//verify Bob
			String issuer = bobCert.getIssuerDN().getName().split(",")[0].split("=")[1];
			if(issuer.equals("bobCert"))vWrite("Alice: Bob's cert verified!");
			else{
				vWrite("Alice: Bob's cert not verified!");
				//exit program
			}
			
			//create nonce
			String nonce = SecurityUtil.createRandomNonce();
			vWrite("Alice: Creating random nonce: " + nonce);
			
			//encrypt nonce 
			String encryptedNonce = SecurityUtil.encrypt(nonce, bobPublicKey);
			vWrite("Alice: Encrypting random nonce: " + encryptedNonce);
			
			//send nonce to Bob
			bobOutputStream.writeBytes(encryptedNonce + "\n");
			vWrite("Alice: Sending Bob Message 3");
			dWrite("Message 3 To Bob  : { (N1)Bob+ }");
			
			//add an entry that will be used by Hash.
			allMessages += encryptedNonce;
			
			//receive Bob's nonce
			vWrite("Alice: Awaiting message 4...");
			String bobNonce = bobInputStream.readLine();
			vWrite("Alice: Received message 4!");
			dWrite("Message 4 From Bob: { (N2)Alice+ }");
			
			//add an entry that will be used by Hash.
			allMessages += bobNonce;
			
			//decrypt Bob's nonce
				//get the key
			PrivateKey alicePrivateKey = SecurityUtil.getPrivateKey("src/resources/updatedkeys/alicePrivateKey.der");
			//display to user
			SecurityUtil.hexPrint(alicePrivateKey.getEncoded(), "Alice: Alice's private key...");
			
			//decrypt using bobPrivate key
			String decryptedNonce = SecurityUtil.decrypt(bobNonce, alicePrivateKey);
			vWrite("Alice: Decrypting message 4: "+ decryptedNonce);
			
			//create final appendage
			//allMessages += "CLIENT";
			//compute hash
			byte[] allMessagesHashed = SecurityUtil.computeHash(allMessages + "CLIENT");
			vWrite("Alice: Computing hash all message hash: " + allMessagesHashed);
			
			//send to Bob
			vWrite("Alice: Sending computed hash. ");
			bobOutputStream.write(allMessagesHashed);
			bobOutputStream.flush();
			dWrite("Message 5 To Bob  : { All Message Hash + CLIENT }");
			
			//Receive Bob's Hash Bytes
			byte[] bobHashBytes = new byte[20];
			vWrite("Alice: Bob's Hash Bytes...");
			bobInputStream.readFully(bobHashBytes);
			dWrite("Message 5 From Bob: { All Message Hash + SERVER }");
			
			//verify Bob
			byte[] bobExpectedHash = SecurityUtil.computeHash(allMessages + "SERVER"); 
			if(Arrays.equals(bobHashBytes, bobExpectedHash)) vWrite("Alice: Bob's Hash is correct!");
			else vWrite("Alice: Bob's Hash isn't correct! ERROR!");
			
			//create master secret nAlice XOR nBob, generate 4 key's
			String masterKey = SecurityUtil.XOR(nonce, decryptedNonce);
			vWrite("Alice: Creating master key: "+ masterKey);
			
			//generate the required 4 keys for encryption and authentication
			SecretKey hashFromBob = SecurityUtil.generateAesKey("toAliceAuth", masterKey);
			SecretKey hashToBob = SecurityUtil.generateAesKey("toBobAuth", masterKey);
			SecretKey encryptionFromBob = SecurityUtil.generateAesKey("toAliceEncryption", masterKey);
			SecretKey encryptionToBob = SecurityUtil.generateAesKey("ToBobEncryption", masterKey);
			vWrite("Alice: 4 AES keys generated for encryption and authentication ");
			dWrite("4 AES keys generated for encryption and authentication");
			
			vWrite("Alice: Handshake Phase completed!");
			dWrite("/***   Handshake Phase complete  ***/ ");
			dWrite("");
			dWrite("/************************************************************ ");
			dWrite("******************Alice Data Phase ************************** ");
			dWrite("/***********************************************************/ ");
			
			/********************************************************************************
			 **************** Data Phase ***********************************************
			 ********************************************************************************/
			vWrite("Alice: Iniating Data Phase...");
			//read in file
			vWrite("Alice: Loading 50kb text file, sample.txt...");
			dWrite("Sending 50kb text file, sample.txt in SSL format...");
			FileInputStream fileIn = new FileInputStream("src/resources/files/sample.txt");
			
			//partition file into smaller pieces and create SSL pieces
			byte[] recordHeader = new byte[8];
			int sequenceNumber = 0; //this will keep track of chunk is being packaged
			byte[] chunk = new byte[1024];
			byte[] hashedChunk = new byte[1036];
			vWrite("Alice: Splitting files into chunks...");
			vWrite("Alice: Preparing and formatting chunks to be SSL blocks...");
			ObjectOutputStream objectWrite = new ObjectOutputStream(bobSocket.getOutputStream());

			int chunkLength = 0;
			//read in all the chunks
			while((chunkLength = fileIn.read(chunk))!=-1){
				//CREATE HMAC
				//giving it a sequence number
				byte[] temp = ByteBuffer.allocate(4).putInt(sequenceNumber).array();
				System.arraycopy(temp, 0, hashedChunk, 0, temp.length);
				
				//create the record header, which is never encrypted
				//record type = 1;
				recordHeader[0] = 1;
				
				//ssl version 3
				recordHeader[1] = 3;
				
				//if end of file, 1 else 0
				if(chunkLength==1024) recordHeader[2] = 0;
				else recordHeader[2] = 1;
				
				//store the length of chunk
				byte[] lengthOfChunk = ByteBuffer.allocate(4).putInt(chunkLength).array();
				System.arraycopy(lengthOfChunk, 0, recordHeader, 3, lengthOfChunk.length);
				
				//record header has 8 elements
				recordHeader[7] = 8;
				
				//prep to hash
				//add the record header
				System.arraycopy(recordHeader, 0, hashedChunk, temp.length, recordHeader.length);
				//add the data
				System.arraycopy(chunk, 0, hashedChunk, temp.length + recordHeader.length, chunk.length);
				
				//perform the hash
				byte[] hmac = SecurityUtil.computeHash(hashedChunk);
				//allocate memory, and then add the hmac and the chunk
				byte[] raw = new byte[hmac.length + chunk.length];
				System.arraycopy(chunk, 0, raw, 0, chunk.length);
				System.arraycopy(hmac, 0, raw, chunk.length, hmac.length);
				
				//encryp
				byte[] encrytedDataAndHmac = SecurityUtil.aesEncrypt(raw, encryptionToBob);
				
				//send to bob
				//allocate the size of array
				byte[] toBob = new byte[recordHeader.length + encrytedDataAndHmac.length];
				//add the content to the array
				System.arraycopy(recordHeader, 0, toBob, 0, recordHeader.length);
				System.arraycopy(encrytedDataAndHmac, 0, toBob, recordHeader.length, encrytedDataAndHmac.length);
			
				//send the completed SSL block to Bob
				//test object write
				objectWrite.writeObject(toBob);

				//bobOutputStream.write(toBob);
				//bobOutputStream.flush();
				
				//increment sequence number
				sequenceNumber++;
				//vWrite("Sequence # " + sequenceNumber); //for debugging
			}
			
			vWrite("Alice: File has finished sending!");
			vWrite("Alice: Bob wrote to a file the data he received. Since this is a demo program, we can read his rendered file and compare!");
			
			//read in the file
			FileInputStream fileIn2 = new FileInputStream("src/resources/files/sampleVerification.txt");
			
			//diff the file
			byte[] sampleText = Files.readAllBytes(Paths.get("src/resources/files/sample.txt"));
			byte[] sampleTextVerify = Files.readAllBytes(Paths.get("src/resources/files/sampleVerification.txt"));
			if(Arrays.equals(sampleText, sampleTextVerify)){
				vWrite("Alice: Bob's file matches the original text!");
				dWrite("Bob's file matches the original text!");
			}
			
			
			vWrite("Alice: File has been securely sent. Thank you, have a nice day!");
			vWrite("Alice: Program terminate.");
			dWrite("Program terminate.");
			//clean up resources
			bobOutputStream.close();
			bobInputStream.close();
			objectWrite.close();
			fileIn.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void vWrite(String txt){
		if(demostration == false) System.out.println(txt);
	}
	
	public static void dWrite(String txt){
		if(demostration == true) System.out.println(txt);
	}

}
