import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.security.cert.X509Certificate;

/**
 * A static class that will help with security tasks such as creating certificates and authentication.
 * @author Daryl
 *
 */
public class SecurityUtil {
	public static KeyStore getKeystore(String path){
		KeyStore temp = null;
		try {
			temp = KeyStore.getInstance("JKS");
			FileInputStream fileInput = new FileInputStream(path);
			char[] password = "changeit".toCharArray();
			temp.load(fileInput, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return temp;
	}
		
	/*
	 * we'll need to print hexidecimal representations of keys, and byte arrays often.
	 */
	public static void hexPrint(byte[] bytes, String element){
		//use a string buffer
		StringBuffer temp = new StringBuffer();
		//turn bytes into a stringbuffer
		for(int i =0; i< bytes.length; i++){
			int mask = 255;
			temp.append(Integer.toHexString(mask & bytes[i]));
		}
		
		 //System.out.println(element + " : " + temp + "\n");		
	}
	
	/*
	 * we'll need to print hexidecimal representations of keys, and byte arrays often.
	 */
	public static String toHexString(byte[] bytes){
		//use a string buffer
		StringBuffer temp = new StringBuffer();
		//turn bytes into a stringbuffer
		for(int i =0; i< bytes.length; i++){
			int mask = 255;
			temp.append(Integer.toHexString(mask & bytes[i]));
		}
		
		return temp.toString();	
	}
	
	public static String encrypt(String unencryptedString, PublicKey pubKey){
		byte[] cipherText = null;
		String returnText = "";
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, pubKey);
			byte[] messageBytes = unencryptedString.getBytes("UTF8");			
			cipherText = cipher.doFinal(messageBytes);
			
			returnText = Base64.getEncoder().encodeToString(cipherText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnText;
	}
	
	public static String decrypt(String encryptedString, PrivateKey privKey){
		byte[] plainText = null;
		byte[] cipherText = null;
		String returnText = "";
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.DECRYPT_MODE, privKey);
			
			cipherText = Base64.getDecoder().decode(encryptedString);
			plainText = cipher.doFinal(cipherText);
			
			returnText = new String(plainText);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnText;
	}
	
	
	/*
	 * Helper method that retireves the PrivateKey
	 */
	public static PrivateKey getPrivateKey(String bobprivatekeyname2) {
		//System.out.println("Getting private key: " + bobprivatekeyname2);
		//put the key into a java file
		File key = new File(bobprivatekeyname2);
		//error check before importing
		if(!key.exists()) {
			System.out.println("ERROR: You're missing the private key! ");
			System.exit(-1);
		}
		
		try{
			//put the bytes into an array
			FileInputStream input = new FileInputStream(key);
			DataInputStream dataInput = new DataInputStream(input);
			int fileLength = (int) key.length();
			byte[] keyByte = new byte[fileLength];
			dataInput.readFully(keyByte);
			//decode them
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyByte);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PrivateKey temp = keyFactory.generatePrivate(keySpec);
			//return the key
			return temp;
			
		}catch(FileNotFoundException e){
			System.out.println(e);
			System.exit(-1);
		}catch(IOException e){
			System.out.println(e);
			System.exit(-1);
		} catch(NoSuchAlgorithmException e){ 
			System.out.println(e);
			System.exit(-1);
		} catch(InvalidKeySpecException e){ 
			e.printStackTrace();
			System.exit(-1);
		}
		return null;
	}
	
	/**
	 * This function follows the specifications in sections 11.5 & 11.6 of the book
	 * It makes sure to pick a random number, and is never repeated.
	 * 
	 * @return a 64 bit nonce in the form of a string
	 */
	public static String createRandomNonce(){
		SecureRandom random = new SecureRandom(); 
		Long challenge = random.nextLong();
		String nonce = Long.toBinaryString(challenge);
		
		//append if less than length of 64
		while(nonce.length() < 64){
			nonce = nonce + "1";
		}
		return nonce;
	}
	
	/**
	 * Takes two strings and produces the XOR answer
	 */
	public static String XOR(String s, String t){
		StringBuilder sb = new StringBuilder();

		for(int i = 0; i < s.length(); i++)
		    sb.append((t.charAt(i) ^ s.charAt(i)));

		return sb.toString();	
	}
	
	public static SecretKey generateAesKey(String password, String masterKey){
		SecretKey returnKey = null;
		try {
			//3DES, using CBC
			byte[] keyBytes = masterKey.getBytes("UTF8");
			KeySpec keySpec = new PBEKeySpec(password.toCharArray(), keyBytes, 65536, 128);
			SecretKey secretKey = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1").generateSecret(keySpec);
			SecretKey superSecretKey = new SecretKeySpec(secretKey.getEncoded(), "AES");
			returnKey = superSecretKey;
			
			} catch (Exception e) {
			e.printStackTrace();
		}
		return returnKey;
	}
	
	/**
	 * This computes the hash of a string using SHA-1
	 */
	public static byte[] computeHash(String message)throws Exception {
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		md.update(message.getBytes(), 0, message.getBytes().length);
		return md.digest();
	}
	
	/*
	 * Overloaded method, same as above
	 */
	public static byte[] computeHash(byte[] message)throws Exception {
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		md.update(message, 0, message.length);
		return md.digest();
	}
	
	/**
	 * Encrypts using the generated AES key
	 * @param plaintext
	 * @param key
	 * @return
	 */
	public static byte[] aesEncrypt(byte[] plaintext, SecretKey key){
		byte[] cipherText = null;
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(new byte[16]));
			cipherText = cipher.doFinal(plaintext);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cipherText;
	}
	
	/**
	 * Encrypts using the generated AES key
	 * @param plaintext
	 * @param key
	 * @return
	 */
	public static byte[] aesDecrypt(byte[] cipherText, SecretKey key){
		byte[] plainText = null;
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(new byte[16]));
			plainText = cipher.doFinal(cipherText);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return plainText;
	}
}
