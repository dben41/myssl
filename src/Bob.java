import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.security.cert.CertificateException;

import java.security.cert.X509Certificate;



public class Bob {
	static Boolean demostration = false;
	public static void main(String[] args) {
		String allMessages = "";
		try {
		//start connection
		vWrite("Bob: Begin program.");
		ServerSocket connectionSocket = new ServerSocket(2112);
		vWrite("Bob: Awaiting connection on port 2112, on ip localhost.");
		Socket serverSocket = connectionSocket.accept();
		vWrite("Bob: Received connection from Alice on port 2112!");
		//open way to talk
		DataOutputStream aliceOutputStream = new DataOutputStream(serverSocket.getOutputStream());
		DataInputStream aliceInputStream = new DataInputStream(serverSocket.getInputStream());
		
		/********************************************************************************
		 **************** Handshake Phase ***********************************************
		 ********************************************************************************/
		dWrite("/************************************************************ ");
		dWrite("******************Alice Handshake Phase********************** ");
		dWrite("/***********************************************************/ ");
		
		//load BobCert
		KeyStore bobKeystore = SecurityUtil.getKeystore("src/resources/updatedkeys/keystoreBob.jks");
		vWrite("Bob: Loading bob keystore: " + bobKeystore.toString());
		Certificate bobCert = bobKeystore.getCertificate("bobCert");
		vWrite("Bob: Loading bob certicate: " + bobCert.getEncoded());
		
		//for debugging
		//byte[] hello = bobCert.getEncoded();
		//int size = hello.length;
		
		//await Alice's cert
		byte[] aliceCertBytes = new byte[650];
		vWrite("Bob: Awaiting message 1, Alice's cert...");
		aliceInputStream.readFully(aliceCertBytes);
		dWrite("Message 1 From Alice: { Alice Certificate, integrity protection:AES, data encryption: SHA-1 }");

		//add an entry that will be used by Hash.
		allMessages += SecurityUtil.toHexString(aliceCertBytes);
		
		InputStream in = new ByteArrayInputStream(aliceCertBytes);
		CertificateFactory certFactory = CertificateFactory.getInstance("X.509");		
		//Certificate aliceCert = certFactory.generateCertificate(in);
		X509Certificate aliceCert = (X509Certificate)certFactory.generateCertificate(in);
		vWrite("Bob: Message 1, Cert received! " + aliceCert.getEncoded());
		
		
		
		vWrite("Bob: Receiving Alice's choice of encryption and integrity...");
		String aliceEncyptionAndIntegrityChoice = aliceInputStream.readLine();
		
		//add an entry that will be used by Hash.
		allMessages += aliceEncyptionAndIntegrityChoice;
				
		vWrite("Bob: Alice has chosen: " + aliceEncyptionAndIntegrityChoice);
		//extract public key from AliceCert
		PublicKey alicePublicKey = aliceCert.getPublicKey();
		vWrite("Bob: Extracting Alice's public key from cert... " + alicePublicKey.getEncoded());
		
		//verify Alice
		String issuer = aliceCert.getIssuerDN().getName().split(",")[0].split("=")[1];
		if(issuer.equals("aliceCert"))vWrite("Bob: Alice's cert verified!");
		else{
			vWrite("Bob: Alice's cert not verified!");
			//exit program
		}
		
		//send Alice the cert
		aliceOutputStream.write(bobCert.getEncoded());
		aliceOutputStream.flush();
		
		//add an entry that will be used by Hash.
		allMessages += SecurityUtil.toHexString(bobCert.getEncoded());
		
		//create nonce
		String nonce = SecurityUtil.createRandomNonce();
		vWrite("Bob: Generating random nonce: " + nonce);
		
		//encrypt nonce 
		String encryptedNonce = SecurityUtil.encrypt(nonce, alicePublicKey);
		vWrite("Bob: Encrypting random nonce: " + encryptedNonce);
				
		//await alice's nonce
		vWrite("Bob: Awaiting Alice's nonce, message 3...");
		String aliceNonce = aliceInputStream.readLine();
		
		//add an entry that will be used by Hash.
		allMessages += aliceNonce;
		
		//send alice the encrypted nonce
		vWrite("Bob: Receied Alice's nonce! Now sending Message 4.");
		aliceOutputStream.writeBytes(encryptedNonce + "\n");
		
		//add an entry that will be used by Hash.
		allMessages += encryptedNonce;
		
		//decrypt Alice's nonce
			//get key
		PrivateKey bobPrivateKey = SecurityUtil.getPrivateKey("src/resources/updatedkeys/bobPrivateKey.der");
		//display to user
		SecurityUtil.hexPrint(bobPrivateKey.getEncoded(), "Bob: Bob's private key...");
		
		//decrypt using bobPrivate key
		String decryptedNonce = SecurityUtil.decrypt(aliceNonce, bobPrivateKey);
		vWrite("Bob: Decrypting message 4 Alice's nonce! " + decryptedNonce);
		
		//receive Alice's Hash
		vWrite("Bob: Receiving computed hash. ");
		
		
		//send Bob's Hash
		byte[] allMessagesHashed = SecurityUtil.computeHash(allMessages + "SERVER");
		vWrite("Bob: Computing hash all message hash: " + allMessagesHashed);
		
		aliceOutputStream.write(allMessagesHashed);
		aliceOutputStream.flush();
		
		//receive Alice's Hash Bytes
		byte[] aliceHashBytes = new byte[20];
		vWrite("Bob: Alice's Hash Bytes...");
		aliceInputStream.readFully(aliceHashBytes);
		
		//verify Alice
		byte[] aliceExpectedHash = SecurityUtil.computeHash(allMessages + "CLIENT123");
		if(Arrays.equals(aliceHashBytes, aliceExpectedHash)) vWrite("Bob: Alice's Hash is correct!");
		else vWrite("Bob: Alice's Hash isn't correct! ERROR!");
		

		//create master secret nAlice XOR nBob, generate 4 key's
		String masterKey = SecurityUtil.XOR(nonce, decryptedNonce);
		vWrite("Bob: Creating master key: "+ masterKey);
		
		//generate the required 4 keys for encryption and authentication
		SecretKey hashToAlice = SecurityUtil.generateAesKey("toAliceAuth", masterKey);
		SecretKey hashFromAlice = SecurityUtil.generateAesKey("toBobAuth", masterKey);
		SecretKey encryptionToAlice = SecurityUtil.generateAesKey("toAliceEncryption", masterKey);
		SecretKey encryptionFromAlice = SecurityUtil.generateAesKey("ToBobEncryption", masterKey);
		vWrite("Bob: 4 AES keys generated for encryption and authentication ");
		
		vWrite("Bob: Handshake Phase completed!");
		
		/********************************************************************************
		 **************** Data Phase ***********************************************
		 ********************************************************************************/
		vWrite("Bob: Iniating Data Phase...");
		
		byte[] recordHeader = new byte[8];
		int sequenceNumber = 0; //this will keep track of chunk is being packaged
		byte[] hashedChunk = new byte[1036];
		boolean endOfFile = false;
		int chunkLength = 1024;
		//Bob will create this, so both can do the diff
		ObjectInputStream objectRead = new ObjectInputStream(serverSocket.getInputStream());
		FileOutputStream fileOut= new FileOutputStream("src/resources/files/sampleVerification.txt");
		
		//loop until the file has bee read completely
		while(endOfFile == false){
			//get the data from Alice 
			
			/*
			//idk how much this'll be, use baos
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] buf = new byte[4096];
			while(true) {
			  int n = aliceInputStream.read(buf);
			  if( n < 0 ) break;
			  baos.write(buf,0,n);
			}

			byte data[] = baos.toByteArray();
			*/
			
			//try getting it using object stream
			
			byte data[] = (byte[])objectRead.readObject();
			
			//get the record heder, first 8 bytes
			recordHeader = Arrays.copyOfRange(data, 0, 8);
			
			//check to see if eof 
			if(recordHeader[2] == 1){
				endOfFile = true;
				//get the last chunk
				chunkLength = ByteBuffer.wrap(Arrays.copyOfRange(data, 3, 7)).getInt(0);			
			}
			
			//get the encrypted bytes
			byte [] cipherBytes = Arrays.copyOfRange(data, 8, data.length);
			byte [] plainBytes = SecurityUtil.aesDecrypt(cipherBytes, encryptionFromAlice);
			
			//get data from SSL block
			byte[] temp = Arrays.copyOfRange(plainBytes, 0, plainBytes.length - 20);
			
			//get the signed HMAC
			byte[] hmac = Arrays.copyOfRange(plainBytes, plainBytes.length - 20, plainBytes.length);
			
			byte[] sequenceNumberInHash = ByteBuffer.allocate(4).putInt(sequenceNumber).array();
			System.arraycopy(sequenceNumberInHash, 0, hashedChunk, 0, sequenceNumberInHash.length);
			
			//use the r header
			System.arraycopy(recordHeader, 0, hashedChunk, sequenceNumberInHash.length, recordHeader.length);
			
			//this part gets hashed
			System.arraycopy(temp, 0, hashedChunk, sequenceNumberInHash.length + recordHeader.length, temp.length);
			
			//create a rehash of variable to ensure that the block is good
			byte[] hmacRehash = SecurityUtil.computeHash(hashedChunk);
			
			//ensure that the hashes match
			if(Arrays.equals(hmacRehash, hmac)){
				byte[] dataForFile = new byte[chunkLength];
				dataForFile = Arrays.copyOfRange(temp, 0, dataForFile.length);
				fileOut.write(dataForFile);
			}else{
				vWrite("Bob: Error: Failed verification of keyed hashes. Possibly due to corruption or changes in one of the handshake message.");
				vWrite("Bob: Program crash at sequence number: " + sequenceNumber);
				System.exit(0);
			}
			
			//increment the sequence
			sequenceNumber++;

		}
		
		
		
		vWrite("Bob: File has finished sending!");
		
		//read in the file
		
				//diff the file
		
		vWrite("Bob: File has been securely sent. Thank you, have a nice day!");
		vWrite("Bob: Program terminate.");
		
		//clean up resources
		aliceOutputStream.close();
		aliceInputStream.close();
		
		
	}catch(Exception e) {
		e.printStackTrace();
	}
	
	} 
	public static void vWrite(String txt){
		System.out.println(txt);
	}
	
	public static void dWrite(String txt){
		if(demostration == true) System.out.println(txt);
	}

}
